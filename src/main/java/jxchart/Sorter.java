package jxchart;

public interface Sorter {
    void sort(int[] toSort);
    String getAlgorithmName();
}
