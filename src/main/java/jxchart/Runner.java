package jxchart;

import java.util.Arrays;

public class Runner {


    public static void main(String[] args) {

        int[] ints =
                ArrayUtils
                        .generateIntegerRandomArray(100);

        System.out.println(Arrays.toString(ints));

        Sorter bubble = new BubbleSorter();
        Sorter insertion = new InsertionSort();
        insertion.sort(ints);
        System.out.println("Po sortowaniu");
        System.out.println(Arrays.toString(ints));


    }
}
