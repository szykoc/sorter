package jxchart;

public class InsertionSort implements Sorter {

    @Override
    public void sort(int[] toSort) {
        for (int i = 1; i < toSort.length; i++) {
            // [5, 1, 2, 4, 5]
            //  j  i
            //     j+1
            for (int j = i - 1; j >= 0 ; j--) {
                if (toSort[j] > toSort[j + 1]){
                   ArrayUtils
                   .swapElements(toSort, j, j +1);
                } else {
                    break;
                }
            }
            // jeśli użwamy z pętla while to pierwszy for musi
            // miec zakres length - 1
           /* int j = i;
            while(j > 0 && toSort[j - 1] > toSort[j]){
                // [2, 5 , 15]
                //   j  i
                jxchart.ArrayUtils.
                        swapElements(toSort, j, j - 1);
                j = j - 1;
            }*/
        }
    }

    @Override
    public String getAlgorithmName() {
        return "Sortowanie przez wstawianie";
    }
}
