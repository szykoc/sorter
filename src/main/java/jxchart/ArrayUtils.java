package jxchart;

import java.util.Random;

public class ArrayUtils {

    public static int[] generateIntegerRandomArray(int length){
        int[] result = new int[length];
        Random random = new Random();
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(result.length * 10);
        }
        return result;
    }

    public static void swapElements(int[] toSort, int j, int i) {
        int indexITemp = toSort[i];
        toSort[i] = toSort[j];
        toSort[j] = indexITemp;
    }
}
